import wordlewise as wordle
import sys
_ANSWERS=set()
def get_answers():
    if len(_ANSWERS):
        return _ANSWERS
    with open('wordleanswers.txt','r') as win:
        for line in win:
            word = line.strip()
            if len(word)==5:
                _ANSWERS.add(word)
    return _ANSWERS

def similiar(word,words, min_match):
    sim = []
    for w in words:
        score = 0
        if w==word:
            continue
        for i in range(0,5):
            if w[i]==word[i]:
                score+=1
        if score >= min_match:
            sim.append(w)
    return sim

def test_start_words(start_words,scorer_class,verbose_report=False,max_failures=None):
    print("Testing start word ",*start_words)
    results = []
    sum = 0
    max_guess = 0
    min_guess = 1000
    failed = 0
    good_sum = 0
    failures = []
    counter = 1
    for answer in get_answers():
        gcount = wordle.generate_guesses(list(start_words),lambda w: wordle.score_word(answer,w),scorer_class)
        #print(counter, answer, gcount)
        results.append((answer,gcount,))
        sum+=gcount
        if gcount > max_guess:
            max_guess = gcount
        if gcount < min_guess:
            min_guess = gcount
        if gcount > 6:
            failures.append(answer)
            failed+=1
            if max_failures and failed >= max_failures:
                total = len(results)
                print("Stopping early, fail rate too high")
                return ((total-failed)/total) * 100.0, failures
        else:
            good_sum+=gcount
        counter+=1
    if verbose_report:
        print("\nFAILURES")
        print("-"*50)
        failures.sort()
        all_words = wordle.get_words()
        for f in failures:
            print(f, similiar(f,all_words,4))
        
        dist = [0] * max_guess
        for r in results:
            dist[r[1]-1]+=1
        print("-"*50)
        print("\nREQUIRED GUESSES DISTRIBUTION")
        print("-"*50)
        di = 1
        for d in dist:
            print(di,d)
            di+=1
        print("-"*50)
    total = len(results)
    print(failed, "failed")
    success_rate = ((total-failed)/total) * 100.0
    print(success_rate, "success rate")
    print(max_guess, "max_guess")
    print(min_guess, "min_guess")
    print(sum/total, "avg_guess")
    print(total, "total")
    good = total - failed
    print(good_sum/good, "average_success")
    return success_rate,failures
    
if __name__=="__main__":
    test_start_words(sys.argv[1:],wordle.MidFreqScorer,True)
