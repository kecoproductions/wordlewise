import wordlewise as wordle
import test_start_word
import sys
# This may take hours or days to run, if you want to run it on multiple machines
# you can specify a word to start with on the command line and it will start testing
# from there
if __name__=="__main__":
    best_word = None
    best_word_score = 0.0
    all_words = wordle.get_words()
    print(len(all_words),"total_words")
    uwords = [word for word in all_words if len(set(word))==5]
    print(len(uwords),"uwords")
    uwords.sort()
    start_from = uwords[0]
    other_start_words = []
    if len(sys.argv) > 1:
        start_from = sys.argv[1]
    if len(sys.argv) > 2:
        other_start_words = sys.argv[2:]
    print(start_from)
    max_failures = 2000
    for word in uwords:
        if word < start_from:
            continue
        testing = other_start_words+[word]
        print("Testing",*testing)
        wscore,failed = test_start_word.test_start_words(testing,wordle.MidFreqScorer,False,max_failures)
        print(word,wscore,best_word,best_word_score)
        if wscore > best_word_score:
            best_word_score = wscore
            best_word = word
            max_failures = len(failed)
