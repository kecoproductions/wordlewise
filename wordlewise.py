import sys
class MidFreqScorer:
    def __init__(self, words):
        freqs = get_freqs(words)
        total_words = len(words)
        lscores = {}
        for l in freqs:
            pt = freqs[l] / total_words
            lscores[l] = abs(pt - 0.5)
        slist = []
        for l in 'abcdefghijklmnopqrstuvwxyz':
            slist.append(lscores.get(l,1.0))
        self.slist = slist

    def calc_word_score(self, word):
        score = 0
        seen = set()
        slist = self.slist.copy()
        for l in word:
            ol = ord(l)-97
            score += slist[ol]
            slist[ol]+=1.0
        return score
    
    def better_score(self, score, best_score):
        return score < best_score


_WWORDS = {}
def get_words():
    if len(_WWORDS)==0:
        with open('wordlewordsfreqs.txt','r') as win:
            for line in win:
                line = line.strip()
                if len(line):
                    _WWORDS[line[:5]]=int(line[6:])
    return set(_WWORDS.keys())

# https://www.kaggle.com/rtatman/english-word-frequency
def get_word_freq(word):
    return _WWORDS.get(word,0)

def get_freqs(words):
    freqs = {}
    for word in words:
        for l in set(word):
            if l in freqs:
                freqs[l]+=1
            else:
                freqs[l]=1
    return freqs

def has_all_letters(word, letters):
    for l in letters:
        if l not in word:
            return False
    return True

def has_any_letters(word, letters):
    for l in letters:
        if l in word:
            return True
    return False

def has_all_positions(word, positions):
    for pos in positions:
        if not word[pos[1]] == pos[0]:
            return False
    return True

def has_any_positions(word, positions):
    for pos in positions:
        if word[pos[1]] == pos[0]:
            return True
    return False

def input_guess_results(guess):
    print("="*50)
    print("GUESS:", guess)
    print("enter the result for each letter then hit enter (example: 02210)")
    print("0 = gray/not in word")
    print("1 = orange/in word, wrong position")
    print("2 = green/in word, right position")
    colors = input(">").strip()
    return colors


def score_word(answer, word):
    colors = ['0', '0', '0', '0', '0']
    alist = list(answer)
    wlist = list(word)
    for i in range(0, 5):
        if wlist[i] == alist[i]:
            colors[i] = '2'
            alist[i] = '*'
    for i in range(0, 5):
        if colors[i] == '2':
            continue
        if wlist[i] in alist:
            colors[i] = '1'
            alist.remove(wlist[i])
        else:
            colors[i] = '0'
    
    return ''.join(colors)

def find_matches(words,good,bad,good_positions,bad_positions):
    words = list(words)
    words.sort()
    matches = []
    idx = {}
    
    for word in words:
        if not has_all_positions(word, good_positions):
            continue
        if has_any_positions(word, bad_positions):
            continue
        if not has_all_letters(word, good):
            continue
        if has_any_letters(word, bad):
            continue
        matches.append(word)
    return matches

def add_constraints(guess, colors, good, bad, good_positions, bad_positions):
    for i in range(0, 5):
        l = guess[i]
        c = colors[i]
        pos = (l,i)
        if c == '0':
            bad.add(l)
            bad_positions.append(pos)  # accounts for a letter that shows up bad because guess had the letter twice but the correct answer only has once
        elif c == '1':
            good.add(l)
            bad_positions.append(pos)
        elif c == '2':
            good.add(l)
            if not pos in good_positions:
                good_positions.append((l, i))
    bad -= good  # accounts for a letter that shows up bad because guess had the letter twice but the correct answer only has once



def generate_guesses(start_words, guess_results_function, scorer_class):
    """
    
    :param start_words: first 1 or more guesses before we have any hints
    :param guess_results_function: function that marks the colors for a guess,
            this is the only information generate_guesses gets about what the answer is.
    :param scorer_class: Scoring algorithm to use to try to make the next best guess
    :return: how many guesses it took to get the right word
    """
    good = set()
    bad = set()
    good_positions = []
    bad_positions = []
    
    words = get_words()
    guess=start_words.pop(0)
    guess_count = 0
    while True:
        guess_count += 1
        matches = []
        colors = guess_results_function(guess) # record what colors the current guess was given
        if colors=="22222": #0=gray, 1=orange 2=green
            return guess_count
        # add in what we learned from the hints
        add_constraints(guess, colors, good, bad, good_positions, bad_positions)
        if len(start_words) and len(good_positions) < 4:
            guess = start_words.pop(0)
        else:
            # find words that match the current hints that we know
            matches = find_matches(words, good, bad, good_positions, bad_positions)
            best_match = None
            best_score = None
            if len(good_positions) < 4 and guess_count!=6:
                scorer = scorer_class(matches)
                for word in matches:
                    score = scorer.calc_word_score(word)
                    if best_score is None:
                        best_score = score
                        best_match = word
                    elif score==best_score:
                        if get_word_freq(word) > get_word_freq(best_match):
                            best_score = score
                            best_match = word
                    elif scorer.better_score(score, best_score):
                        best_score = score
                        best_match = word
            # if we know all but one letter pick the most common word
            else:
                best_score = -1
                for word in matches:
                    score = get_word_freq(word)
                    if score > best_score:
                        best_score = score
                        best_match = word

            #print("Narrowed to",len(matches),"matches")
            #print("-"*50)
            guess=best_match
            words = matches
    
if __name__=="__main__":
    start_words = ["blond"]
    if len(sys.argv) > 1:
        start_words = sys.argv[1:]
    guess_count = generate_guesses(start_words,input_guess_results, MidFreqScorer)
    print("You took",guess_count,"guesses")
