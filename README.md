# WordleWise

This is a program to make guesses for Wordle challenges as found at https://www.powerlanguage.co.uk/wordle/

It requires python version 3 or greater.

Currently it able to win Wordle in normal mode 99.8% of the time using the start words:  gland tromp chibs

To use these start words, run wordlewise.py like this
```
python wordlewise.py gland tromp chibs
```
You will be prompted to enter the results of each guess and then be told what to guess next

In hard mode, this program is able to win Wordle 99% of the time using the start word: blond

To do this, run wordlewise.py like this:

```
python wordlewise.py blond
```

There are several other files of interest in this directory

 * wordlewordfreqs.txt - An alphabetical list of all the words Wordle allows as guesses along with their frequency of usage according to https://www.kaggle.com/rtatman/english-word-frequency
 * wordleanswers.txt - This is a subset of wordlewordfreqs and is the set of words that Wordle uses for answers.  This is only used for testing how well an algorithm does and how well different start words work
 * test_start_words.py - Test how well one or more start words do overall for all answers
 * find_best_wordle_start_word.py - This tests all legal words and sees how well they do. This may take hours or days to run
